# Docker

Docker is an open source tool designed to make it easier to create, deploy, and run applications by using 
containers. Containers allow a developer to package up an application with all of the parts it needs, such as 
libraries and other dependencies, and deploy it as one package.

Docker is a bit like a virtual machine. But unlike a virtual machine, rather than creating a whole virtual 
operating system, Docker allows applications to use the same Linux kernel as the system that they're running 
on and only requires applications be shipped with things not already running on the host computer.

Developers can focus on writing code without worrying about the system that it will ultimately be running on. 
It also allows them to get a head start by using one of thousands of programs already designed to run in a 
Docker container as a part of their application.

Docker brings security to applications running in a shared environment, but containers by themselves are not 
an alternative to taking proper security measures.

![](https://www.docker.com/sites/default/files/social/docker_facebook_share.png)