# GIT (Global information Tracker)

Git is an Open Source **Distributed Version Control System**.
It is designed to handle everything from small to very large projects with speed and efficiency.

It provides various feature:

**Strong support for non-linear development**
Git supports rapid branching and merging, and includes specific tools for visualizing and navigating a 
non-linear development history.

**Distributed development**
Git gives each developer a local copy of the full development history, and changes are copied from one such 
repository to another. These changes are imported as added development branches and can be merged in the same 
way as a locally developed branch.

**Efficient handling of large projects**
Git is fast and scalable, faster than some version-control systems; fetching version history from a locally 
stored repository can be one hundred times faster than fetching it from the remote server.

![](https://d1jnx9ba8s6j9r.cloudfront.net/blog/wp-content/uploads/2016/11/Git-In-Devops-What-Is-Git-Edureka-2.png)